### Run application:

* ./mvnw spring-boot:run

Or 
* maven package
* java -jar target/meetings-proposer-0.0.1-SNAPSHOT.jar


**** 
[Official Apache Maven documentation](https://maven.apache.org/guides/index.html)

