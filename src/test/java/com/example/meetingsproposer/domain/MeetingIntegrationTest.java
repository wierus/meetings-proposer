package com.example.meetingsproposer.domain;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
class MeetingIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void givenCorrectJsonAndQueryParam_returns200() throws Exception {
        String json = "[\n" +
                "    {\n" +
                "        \"working_hours\": {\n" +
                "            \"start\": \"09:00\",\n" +
                "            \"end\": \"19:55\"\n" +
                "        },\n" +
                "        \"planned_meeting\": [\n" +
                "            {\n" +
                "                \"start\": \"09:00\",\n" +
                "                \"end\": \"10:30\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"start\": \"12:00\",\n" +
                "                \"end\": \"13:00\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"start\": \"16:00\",\n" +
                "                \"end\": \"18:00\"\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    {\n" +
                "        \"working_hours\": {\n" +
                "            \"start\": \"10:00\",\n" +
                "            \"end\": \"18:30\"\n" +
                "        },\n" +
                "        \"planned_meeting\": [\n" +
                "            {\n" +
                "                \"start\": \"10:00\",\n" +
                "                \"end\": \"11:30\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"start\": \"12:30\",\n" +
                "                \"end\": \"14:30\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"start\": \"14:30\",\n" +
                "                \"end\": \"15:00\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"start\": \"16:00\",\n" +
                "                \"end\": \"17:00\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "]";
        String jsonExpected = "[\n" +
                "    {\n" +
                "        \"start\": \"11:30:00\",\n" +
                "        \"end\": \"12:00:00\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"start\": \"15:00:00\",\n" +
                "        \"end\": \"16:00:00\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"start\": \"18:00:00\",\n" +
                "        \"end\": \"18:30:00\"\n" +
                "    }\n" +
                "]";
        int meetingDurationMin = 30;

        mockMvc.perform(post("/suggestHours")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .queryParam("duration", String.valueOf(meetingDurationMin)))
                .andExpect(status().isOk())
                .andExpect(content().json(jsonExpected));
    }


    @Test
    void findPossibleMeetingsHours() throws Exception {
        String json = "[\n" +
                "    {\n" +
                "        \"working_hours\": {\n" +
                "            \"start\": \"09:00\",\n" +
                "            \"end\": \"19:55\"\n" +
                "        },\n" +
                "        \"planned_meeting\": [\n" +
                "            {\n" +
                "                \"start\": \"09:30\",\n" +
                "                \"end\": \"10:30\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"start\": \"12:00\",\n" +
                "                \"end\": \"13:00\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"start\": \"16:00\",\n" +
                "                \"end\": \"18:00\"\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    {\n" +
                "        \"working_hours\": {\n" +
                "            \"start\": \"08:00\",\n" +
                "            \"end\": \"18:30\"\n" +
                "        },\n" +
                "        \"planned_meeting\": [\n" +
                "            {\n" +
                "                \"start\": \"10:00\",\n" +
                "                \"end\": \"11:30\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"start\": \"12:30\",\n" +
                "                \"end\": \"14:30\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"start\": \"14:30\",\n" +
                "                \"end\": \"15:00\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"start\": \"16:00\",\n" +
                "                \"end\": \"17:00\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "]";
        String jsonExpected = "[\n" +
                "    {\n" +
                "        \"start\": \"15:00:00\",\n" +
                "        \"end\": \"16:00:00\"\n" +
                "    }\n" +
                "]";
        int meetingDurationMin = 40;

        mockMvc.perform(post("/suggestHours")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .queryParam("duration", String.valueOf(meetingDurationMin)))
                .andExpect(status().isOk())
                .andExpect(content().json(jsonExpected));
    }
}