package com.example.meetingsproposer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MeetingsProposerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MeetingsProposerApplication.class, args);
    }

}
