package com.example.meetingsproposer.api.controller;

import com.example.meetingsproposer.api.dto.CalendarDto;
import com.example.meetingsproposer.domain.Meeting;
import com.example.meetingsproposer.domain.MeetingService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MeetingsController {

    private final MeetingService meetingService;

    public MeetingsController(MeetingService meetingService) {
        this.meetingService = meetingService;
    }

    @PostMapping(path = "/suggestHours")
    public List<Meeting> getPossibleMeetingHours(@RequestBody List<CalendarDto> calendarDtoList,
                                                 @RequestParam(name = "duration") int durationInMin) {
        return meetingService.suggestMeetingHours(calendarDtoList, durationInMin);
    }
}
