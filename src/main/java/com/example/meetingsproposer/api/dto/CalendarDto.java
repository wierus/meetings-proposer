package com.example.meetingsproposer.api.dto;

import com.example.meetingsproposer.domain.Meeting;
import com.example.meetingsproposer.domain.WorkingHours;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CalendarDto {

    @JsonProperty("working_hours")
    private WorkingHours workingHours;
    @JsonProperty("planned_meeting")
    private List<Meeting> plannedMeetings;

    public CalendarDto() {
    }

    public CalendarDto(WorkingHours workingHours, List<Meeting> plannedMeetings) {
        this.workingHours = workingHours;
        this.plannedMeetings = plannedMeetings;
    }

    public WorkingHours getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(WorkingHours workingHours) {
        this.workingHours = workingHours;
    }

    public List<Meeting> getPlannedMeetings() {
        return plannedMeetings;
    }

    public void setPlannedMeetings(List<Meeting> plannedMeetings) {
        this.plannedMeetings = plannedMeetings;
    }

    @Override
    public String toString() {
        return "CalendarDto{" +
                "workingHours=" + workingHours +
                ", plannedMeetings=" + plannedMeetings +
                '}';
    }
}
