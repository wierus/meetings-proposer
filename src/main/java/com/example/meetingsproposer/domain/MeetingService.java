package com.example.meetingsproposer.domain;

import com.example.meetingsproposer.api.dto.CalendarDto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

@Service
public class MeetingService {

    protected final Logger log = Logger.getLogger(getClass().getName());

    /**
     * Finds time slots for meeting according to meeting duration and the calendars of two participants
     *
     * @param calendars            list of 2 different personal calendars
     * @param meetingDurationInMin duration of meeting in minutes
     * @return list of suggested hours for meeting
     **/
    public List<Meeting> suggestMeetingHours(List<CalendarDto> calendars, int meetingDurationInMin) {
        verifyListLength(calendars);

        WorkingHours commonWorkingHours = getCommonWorkingHours(calendars);
        CalendarDto firstPersonCalendar = calendars.get(0);
        List<Meeting> secondPersonMeetings = calendars.get(1).getPlannedMeetings();

        List<Meeting> firstPersonUnoccupiedTime = findUnoccupiedTime(firstPersonCalendar, meetingDurationInMin);
        List<Meeting> possibleHours = trimToCommonWorkingHours(firstPersonUnoccupiedTime, commonWorkingHours);

        Iterator<Meeting> iterator = possibleHours.iterator();
        while (iterator.hasNext()) {
            Meeting freeTime = iterator.next();
            for (Meeting meeting : secondPersonMeetings) {
                if (!meeting.getStart().isBefore(freeTime.getStart())
                        && meeting.getStart().isBefore(freeTime.getEnd())) {
                    freeTime.setStart(meeting.getEnd());
                }
                if (!meeting.getEnd().isBefore(freeTime.getStart()) && meeting.getEnd().isBefore(freeTime.getEnd())) {
                    freeTime.setStart(meeting.getEnd());
                }
            }
            if ((freeTime.getEnd().toSecondOfDay() - freeTime.getStart().toSecondOfDay()) / 60 < meetingDurationInMin) {
                iterator.remove();
            }
        }

        log.info("Suggested hours " + possibleHours.toString());
        return possibleHours;
    }

    private void verifyListLength(List<CalendarDto> list) {
        if (list.size() != 2) {
            throw new IllegalArgumentException("Invalid number of calendars, should contain 2 calendars");
        }
    }

    private WorkingHours getCommonWorkingHours(List<CalendarDto> calendars) {
        WorkingHours workingHours1 = calendars.get(0).getWorkingHours();
        WorkingHours workingHours2 = calendars.get(1).getWorkingHours();
        WorkingHours commonHours = new WorkingHours();

        if (workingHours1.getStart().isAfter(workingHours2.getStart())) {
            commonHours.setStart(workingHours1.getStart());
        } else commonHours.setStart(workingHours2.getStart());

        if (workingHours1.getEnd().isBefore(workingHours2.getEnd())) {
            commonHours.setEnd(workingHours1.getEnd());
        } else commonHours.setEnd(workingHours2.getEnd());

        return commonHours;
    }

    private List<Meeting> trimToCommonWorkingHours(List<Meeting> unoccupiedTime, WorkingHours commonWorkingHours) {
        Iterator<Meeting> iterator = unoccupiedTime.iterator();
        while (iterator.hasNext()) {
            Meeting meeting = iterator.next();
            if (!meeting.getEnd().isAfter(commonWorkingHours.getStart())) {
                unoccupiedTime.remove(meeting);
            } else if (!meeting.getStart().isBefore(commonWorkingHours.getEnd())) {
                unoccupiedTime.remove(meeting);
            } else if (meeting.getStart().isBefore(commonWorkingHours.getStart())) {
                meeting.setStart(commonWorkingHours.getStart());
            } else if (meeting.getEnd().isAfter(commonWorkingHours.getEnd())) {
                meeting.setEnd(commonWorkingHours.getEnd());
            }
        }
        return unoccupiedTime;
    }

    private List<Meeting> findUnoccupiedTime(CalendarDto calendar, int durationInMin) {
        List<Meeting> unoccupiedTime = new ArrayList<>();
        WorkingHours workingHours = calendar.getWorkingHours();
        List<Meeting> plannedMeetings = calendar.getPlannedMeetings();
        Meeting firstMeeting = plannedMeetings.get(0);
        Meeting lastMeeting = plannedMeetings.get(plannedMeetings.size() - 1);

        if (firstMeeting.getStart().isAfter(workingHours.getStart())) {
            int freeMinutesBeforeFirstMeeting = (firstMeeting.getStart().toSecondOfDay() -
                    workingHours.getStart().toSecondOfDay()) / 60;
            if (freeMinutesBeforeFirstMeeting >= durationInMin) {
                unoccupiedTime.add(new Meeting(workingHours.getStart(), firstMeeting.getStart()));
            }
        }
        for (int i = 0; i < plannedMeetings.size() - 1; i++) {
            if ((plannedMeetings.get(i + 1).getStart().toSecondOfDay() -
                    plannedMeetings.get(i).getEnd().toSecondOfDay()) / 60 >= durationInMin) {
                unoccupiedTime.add(new Meeting(plannedMeetings.get(i).getEnd(), plannedMeetings.get(i + 1).getStart()));
            }
        }
        if (lastMeeting.getEnd().isBefore(workingHours.getEnd())) {
            int freeMinutesAfterLastMeeting = (workingHours.getEnd().toSecondOfDay() -
                    lastMeeting.getEnd().toSecondOfDay()) / 60;
            if (freeMinutesAfterLastMeeting >= durationInMin) {
                unoccupiedTime.add(new Meeting(lastMeeting.getEnd(), workingHours.getEnd()));
            }
        }
        return unoccupiedTime;
    }
}
