package com.example.meetingsproposer.domain;

import java.time.LocalTime;

public class Meeting {

    private LocalTime start;
    private LocalTime end;

    public Meeting() {
    }

    public Meeting(LocalTime start, LocalTime end) {
        this.start = start;
        this.end = end;
    }

    public LocalTime getStart() {
        return start;
    }

    public void setStart(LocalTime start) {
        this.start = start;
    }

    public LocalTime getEnd() {
        return end;
    }

    public void setEnd(LocalTime end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "start='" + start + '\'' +
                ", end='" + end + '\'' +
                '}';
    }
}
